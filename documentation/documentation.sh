#!/bin/bash

DOC_SOURCE_DIR=/tmp/project/docs/source

inotify(){
    inotifywait --quiet --recursive --monitor --event modify,delete,move,create --excludei "$DOC_SOURCE_DIR/[^/]+/(build|output)" $DOC_SOURCE_DIR | while read; do build; done
}

build(){
    cd $DOC_SOURCE_DIR
    poetry run make all
    rm -rf /html/public
    mv $DOC_SOURCE_DIR/sphinx/build/html /html/public
    mv $DOC_SOURCE_DIR/smithy/build/openapi-conversion/openapi/Sys.openapi.json /html/public/_static/
    chown www-data -R /html/public
    poetry run make clean
    cd -
}

cd $DOC_SOURCE_DIR; poetry install --no-root; cd -
build

touch /tmp/nginx-access.log /tmp/nginx-err.log
chmod a+rw /tmp/nginx-access.log /tmp/nginx-err.log

inotify &>>/tmp/builder.log &
nginx -c /html/nginx.conf &

tail -f /tmp/builder.log /tmp/nginx-err.log /tmp/nginx-access.log
