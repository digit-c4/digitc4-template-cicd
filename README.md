# digitC4-template-cicd

This Repostory will contain default gitlab ci configuration which should be included inside your gitlab ci/cd configuration.
Example:
`
# Set the default tags where to run the checks
```
default:
  tags:
    - lab
    - shell
```

# you need always to define the stage `default_validate` before yours
```
stages:
  - default_validate
  - ...
```

# include the template
```
include:
  - project: 'digit-c4/digitc4-template-cicd'
    file: 'gitlab-ci-ansible.yml'
    ref: main
```

# Work locally with the same configuration
You should have the same configuration locally to avoid any issues with the CI/CD pipeline.

## Documentation

The documentation to be built should be stored in the `docs/source` folder. Inside, create a Makefile to build the documentation and to clean the environment. Store Sphinx files under docs/source/sphinx and smithy documentation under docs/source/smithy.

For the documentation, you can use the following command to run the same checks locally:
```
cd documentation
export PROJECT_PATH=/path/to/your/project
docker compose up
```
If you need to set proxy environment, set the following environment variables before calling 'docker compose up'. The container will use them:
- http_proxy
- https_proxy
- SMITHY_PROXY_HOST
- SMITHY_PROXY_CREDENTIALS

The documentation will be available at http://localhost:8080 and will autobuild if you modify the files inside the docs/source folder.
